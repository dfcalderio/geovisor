<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * LayerGroup
 *
 * @ORM\Table(name="layer_group")
 * @ORM\Entity
 */
class LayerGroup
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="layer_group_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=50, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="ltype", type="string", length=1, nullable=false)
     */
    private $ltype;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return LayerGroup
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set ltype
     *
     * @param string $ltype
     * @return LayerGroup
     */
    public function setLtype($ltype)
    {
        $this->ltype = $ltype;

        return $this;
    }

    /**
     * Get ltype
     *
     * @return string 
     */
    public function getLtype()
    {
        return $this->ltype;
    }
}
