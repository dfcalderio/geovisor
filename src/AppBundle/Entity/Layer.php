<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Layer
 *
 * @ORM\Table(name="layer", indexes={@ORM\Index(name="IDX_E4DB211AFEB80403", columns={"id_layer_group"})})
 * @ORM\Entity
 */
class Layer
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="layer_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=50, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=100, nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=250, nullable=false)
     */
    private $url;
    
    /**
     * @var string
     *
     * @ORM\Column(name="style", type="string", length=250, nullable=false)
     */
    private $style;
    
    /**
     * @var string
     *
     * @ORM\Column(name="format", type="string", length=50, nullable=false)
     */
    private $format;
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="version", type="string", length=10, nullable=false)
     */
    private $version;
    

    /**
     * @var \LayerGroup
     *
     * @ORM\ManyToOne(targetEntity="LayerGroup")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_layer_group", referencedColumnName="id")
     * })
     */
    private $idLayerGroup;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Layer
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Layer
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return Layer
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set style
     *
     * @param string $style
     * @return Layer
     */
    public function setStyle($style)
    {
        $this->style = $style;

        return $this;
    }

    /**
     * Get style
     *
     * @return string 
     */
    public function getStyle()
    {
        return $this->style;
    }

    /**
     * Set format
     *
     * @param string $format
     * @return Layer
     */
    public function setFormat($format)
    {
        $this->format = $format;

        return $this;
    }

    /**
     * Get format
     *
     * @return string 
     */
    public function getFormat()
    {
        return $this->format;
    }

    /**
     * Set version
     *
     * @param string $version
     * @return Layer
     */
    public function setVersion($version)
    {
        $this->version = $version;

        return $this;
    }

    /**
     * Get version
     *
     * @return string 
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * Set idLayerGroup
     *
     * @param \AppBundle\Entity\LayerGroup $idLayerGroup
     * @return Layer
     */
    public function setIdLayerGroup(\AppBundle\Entity\LayerGroup $idLayerGroup = null)
    {
        $this->idLayerGroup = $idLayerGroup;

        return $this;
    }

    /**
     * Get idLayerGroup
     *
     * @return \AppBundle\Entity\LayerGroup 
     */
    public function getIdLayerGroup()
    {
        return $this->idLayerGroup;
    }
}
