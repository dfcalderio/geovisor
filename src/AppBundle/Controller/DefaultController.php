<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\LayerGroup;

class DefaultController extends Controller {

    /**
     * @Route("/", name="index")
     * @Template
     */
    public function indexAction() {
        return array();
    }
    
    /**
     * @Route("/predio/{clave}", name="informacion-predio",options={"expose"=true})
     * @Template
     */
    public function infoPredioAction($clave) {
        sleep(2);
        return array();
    }

    /**
     * @Route("/admin", name="admin")
     * @Template
     */
    public function adminAction() {
        return array();
    }

    /**
     * @Route("/admin/layer-group/create", name="layer-group-create")
     */
    public function createLayerGroupAction() {

        $request = Request::createFromGlobals();
        $groupName = $request->request->get('groupName');
        $groupType = $request->request->get('groupType');

        $group = new LayerGroup();
        $group->setName($groupName);
        $group->setLtype($groupType);
       
        $em = $this->getDoctrine()->getManager();

        $em->persist($group);
        $em->flush();
    }

}
