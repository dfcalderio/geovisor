window.mapManager = null;
window.coordinates = null;

$(document).ready(function () {
   $.cookieBar({
        message: 'Este sitio usa cookies para guardar datos de preferencias',
        acceptButton: true,
        acceptText: 'Aceptar',
        declineButton: true,
        declineText: 'Desabilitar Cookies',
        expireDays: 365,
        renewOnVisit: false,
        zindex: 300,
        forceShow: false,
        effect: 'slide',
        element: 'body',
        append: false,
        fixed: true,
        bottom: false
    });
    
    $('#weather').weatherfeed(['375744'],{
		woeid: true
	});
    
    // Setup the off-canvas toggle button for smaller screens.
    $("[data-toggle=offcanvas]").click(function () {
        $(".row-offcanvas").toggleClass('active');
    });


    mapManager = new MapManager('map');
    //mapManager.adicionarCapa('batimetria', 0);
    mapManager.adicionarCapa('ortofoto', 1);



    $('#layersPanel a.layer-btn').click(function (e) {
        e.preventDefault();
        var anchor = $(this);
        layer = anchor.attr('data-layer-name');
        index = anchor.attr('data-layer-index');
        color = anchor.attr('data-marker-color');
        
        var anchorContainer = anchor.parent();
        if (anchorContainer.hasClass('layer-off')) {
            anchorContainer.addClass('layer-on');
            anchorContainer.removeClass('layer-off');
            if (layer) {
                mapManager.adicionarCapa(layer, index,color);
                //mapManager.addPrediosLayer();
            }
        } else if (anchorContainer.hasClass('layer-on')) {
            anchorContainer.addClass('layer-off');
            anchorContainer.removeClass('layer-on');
            if (layer) {

                mapManager.removeLayer(layer);
            }
        }
    });


});



/** 
 * Set the height of the map div.
 */
function setMapDivHeight() {
    var topNavBar, mapDiv, desiredHeight, sidebarDiv;

    topNavBar = document.getElementById("topNavBar");
    mapDiv = document.getElementById("map");
    sidebarDiv = document.getElementById("sidebar");

    desiredHeight = window.innerHeight - topNavBar.clientHeight - 40;
    desiredHeight = [desiredHeight, "px"].join("");

    mapDiv.style.height = desiredHeight;
    sidebarDiv.style.height = desiredHeight;

    //var tabPanes = document.querySelectorAll(".tab-pane");

    desiredHeight = window.innerHeight - topNavBar.clientHeight - 80;
    desiredHeight = [desiredHeight, "px"].join("");

    /*for (var i = 0, l = tabPanes.length; i < l; i += 1) {
        tabPanes[i].style.height = desiredHeight;
    }*/
    width = window.innerWidth;
    
}

// Setup map resizing code handlers for when the browser is resized or the device is rotated.
setMapDivHeight();
window.addEventListener("resize", setMapDivHeight, true);
window.addEventListener("deviceorientation", setMapDivHeight, true);

/*
 var selectedFeature = null;
 
 // select interaction working on "pointermove"
 var selectPointerMove = new ol.interaction.Select({
 condition: ol.events.condition.pointerMove
 });
 map.addInteraction(selectPointerMove);
 selectPointerMove.on('select', function (e) {
 if (e.target.getFeatures().getLength() > 0) {
 selectedFeature = e.target.getFeatures().item(0);
 // console.log(e.target.getFeatures().item(0).getProperties().area);
 } else {
 selectedFeature = null;
 }
 
 });
 
 
 
 
 
 */
/*
 var newLayer = new ol.layer.Tile({
 source: new ol.source.OSM()
 });
 
 var vectorLayer = new ol.layer.Tile({
 source: new ol.source.TileWMS({
 preload: Infinity,
 url: 'http://localhost:8080/geoserver/wms',
 serverType: 'geoserver',
 params: {
 'LAYERS': "Salinas:predios", 'TILED': true
 }
 })
 });*/

//map.addLayer(newLayer);
//map.addLayer(vectorLayer);

















//center: ol.proj.transform([-75.923853, 45.428736], 'EPSG:4326', 'EPSG:3857'),
