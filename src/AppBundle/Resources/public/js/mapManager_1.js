var MapManager = function (container_id) {

    this.center_900913 = [-9014470.335882477, -245298.97016348102];
    this.center_32717 = [502521.1127400292, 9756603.191879256];
    this.center_3857 = [-9014064.264169712, -245633.38216222863];
    this.center_4326 = [-80.97826825012811, -2.202897189983574];

    proj4.defs("EPSG:32717", "+proj=utm +zone=17 +south +datum=WGS84 +units=m +no_defs");
    this.proj_32717 = new ol.proj.Projection({
        code: 'EPSG:32717',
        extent: [441867.78, 1116915.04, 833978.56, 10000000.00]
    });


    var map_center = null;

    if (Cookies.getJSON('map_center')) {
        map_center = Cookies.getJSON('map_center').valor;
    }
    var map_resolution = null;
    if (Cookies.getJSON('map_resolution')) {
        map_resolution = Cookies.getJSON('map_resolution').valor;
    }
    if (!map_center) {
        map_center = this.center_3857;
    }

    var map_view = new ol.View({
        projection: Constants.map.srsname,
        center: map_center
    });
    if (!map_resolution) {
        map_view.setZoom(14);
    } else {
        map_view.setResolution(map_resolution);
    }
    this.map = new ol.Map({
        target: container_id,
        renderer: 'canvas',
        view: map_view
    });


    //Full Screen
    var fullBtn = document.createElement('i');
    fullBtn.className = "icon-expand";
    var myFullScreenControl = new ol.control.FullScreen({
        label: fullBtn
    });
    this.map.addControl(myFullScreenControl);

    //Rotate
    var myRotateControl = new ol.control.Rotate();
    this.map.addControl(myRotateControl);

    //ScaleLine
    var myScaleLine = new ol.control.ScaleLine();
    this.map.addControl(myScaleLine);
    //I often use the scale line. The default implementation looks nice.

    //Zoom
    var zoomInBtn = document.createElement('i');
    zoomInBtn.className = "icon-zoom-in";
    var zoomOutBtn = document.createElement('i');
    zoomOutBtn.className = "icon-zoom-out";
    var myZoom = new ol.control.Zoom({
        zoomInLabel: zoomInBtn,
        zoomOutLabel: zoomOutBtn
    });
    this.map.addControl(myZoom);
//Zoom is a default control, but there are some parameters you could change if you wanted:
//Check them out here: http://ol3js.org/en/master/apidoc/ol.control.Zoom.html


//ZoomSlider
    /*
     var myZoomSlider = new ol.control.ZoomSlider();
     map.addControl(myZoomSlider);
     */
//The zoom slider is a nice addition to your map. It is wise to have it accompany your zoom buttons.
    this.selectedFeature = null;


    this.map.getView().on('change:resolution', function (e) {
        Cookies.set('map_resolution', {valor: e.target.get(e.key)});
    });

    this.map.getView().on('change:center', function (e) {
        Cookies.set('map_center', {valor: e.target.get(e.key)});
    });

    this.map.on('singleclick', function (evt) {
        var coord = evt.coordinate;
        var transformed_coordinate = ol.proj.transform(coord, "EPSG:900913", "EPSG:4326");
        //console.log(transformed_coordinate);
        /*if (selectedFeature != null) {
         console.log(selectedFeature.getProperties().area);
         }*/
        console.log(transformed_coordinate);
    })


};
MapManager.prototype.findLayer = function (title) {
    var a = null;
    this.map.getLayers().forEach(function (layer) {
        var layer_title = layer.get('title');
        if (layer_title && layer_title === title) {
            a = layer;
        }
    });
    return a;
};

MapManager.prototype.addLayer = function (layer_name, index) {

    var layer = null;
    switch (layer_name) {
        case 'predios':
            layer = this.createPrediosLayer(index);
            break;
        case 'hospitales':
            layer = this.createHostitalesLayer(index);
            
            break;
        case 'osm':
            layer = new ol.layer.Tile({
                source: new ol.source.OSM()
            });
           
            break;
    }
    if (layer)
    {
        this.map.addLayer(layer)
    }

};
MapManager.prototype.removeLayer = function (layer_name) {
    var layer = this.findLayer(layer_name);
    if (layer) {
        this.map.removeLayer(layer);
    }
};

MapManager.prototype.loadPrediosData = function (response) {
    var layer = this.findLayer('predios');
    if (layer) {
        var source = layer.getSource();
        if (source) {
            var geojsonFormat = new ol.format.GeoJSON();
            source.addFeatures(geojsonFormat.readFeatures(response));
        }
    }
};

MapManager.prototype.createPrediosLayer = function (index) {
    var vectorSource = new ol.source.Vector({
        loader: function (extent, resolution, projection) {
            var url = Constants.geoserver.url + 'wfs?service=WFS&' +
                    'version=1.1.0&request=GetFeature&typename=' + Constants.geoserver.workspace + ':predios&' +
                    'outputFormat=text/javascript&format_options=callback:mapManager.loadPrediosData' +
                    '&srsname=' + Constants.map.srsname + '&bbox=' + extent.join(',') + ',' + Constants.map.srsname;
            // use jsonp: false to prevent jQuery from adding the "callback"
            // parameter to the URL
            $.ajax({url: url, dataType: 'jsonp', jsonp: false});
        },
        strategy: ol.loadingstrategy.bbox
                /*strategy: ol.loadingstrategy.tile(ol.tilegrid.createXYZ({
                 maxZoom: 19
                 }))*/
    });

    vectorSource.on('tileloadstart', function (event) {
        console.log('tileloadstart')
    });

    vectorSource.on('tileloadend', function (event) {
        console.log('tileloadend');
    });
    vectorSource.on('tileloaderror', function (event) {
        console.log('tileloaderror');
    });

    var predios = new ol.layer.Vector({
        source: vectorSource,
        title: 'predios',
        index:index,
        style: new ol.style.Style({
            stroke: new ol.style.Stroke({
                color: 'rgba(0, 0, 255, 1.0)',
                width: 2
            })
        })
    });
    return predios;
};

MapManager.prototype.createHostitalesLayer = function () {

    var geojsonObject = {
        'type': 'FeatureCollection',
        'crs': {
            'type': 'name',
            'properties': {
                'name': 'EPSG:3857'
            }
        },
        'features': [
            {
                'type': 'Feature',
                'geometry': {
                    'type': 'Point',
                    'coordinates': [-9014838.189081099, -244935.8942791264]
                }
            },
            {
                'type': 'Feature',
                'geometry': {
                    'type': 'Point',
                    'coordinates': [-9013901.835484605, -245776.7015902633]
                }
            },
            {
                'type': 'Feature',
                'geometry': {
                    'type': 'Point',
                    'coordinates': [-9012812.607831541, -245853.1386185485]
                }
            }
        ]
    };

    var vectorSource = new ol.source.Vector({
        features: (new ol.format.GeoJSON()).readFeatures(geojsonObject)
    });

    var image = new ol.style.Circle({
        radius: 5,
        fill: null,
        stroke: new ol.style.Stroke({color: 'red', width: 1})
    });

    var style = new ol.style.Style({
        text: new ol.style.Text({
            text: '\uf0fd',
            font: 'normal 24px FontAwesome',
            textBaseline: 'Bottom',
            fill: new ol.style.Fill({
                color: 'red',
            })
        })
    });

    var hospitales = new ol.layer.Vector({
        source: vectorSource,
        title: 'hospitales',
        index:index,
        style: style
    });
    return hospitales;
};






