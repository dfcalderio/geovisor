var MapManager = function (container_id) {

    this.center_900913 = [-9014470.335882477, -245298.97016348102];
    this.center_32717 = [502521.1127400292, 9756603.191879256];
    this.center_3857 = [-9014064.264169712, -245633.38216222863];
    this.center_4326 = [-80.97826825012811, -2.202897189983574];
    this.visibleLayers = {};
    proj4.defs("EPSG:32717", Constants.proj4js.epsg_32717);
    /*
     this.proj_32717 = new ol.proj.Projection({
     code: 'EPSG:32717',
     extent: [441867.78, 1116915.04, 833978.56, 10000000.00]
     });
     */

    this.defaultLoadParameters = {
        service: 'WFS',
        version: '1.1.0',
        request: 'getFeature',
        srsname: Constants.map.srsname,
        outputFormat: 'text/javascript',
        style: 'zonas_homogeneas'
    };
    var map_center_lat = (Cookies.getJSON('map_center_lat')) ? Cookies.getJSON('map_center_lat') : -2.202897189983574;
    var map_center_lng = (Cookies.getJSON('map_center_lng')) ? Cookies.getJSON('map_center_lng') : -80.97826825012811;
    var map_center = new L.LatLng(map_center_lat, map_center_lng);
    var map_zoom = (Cookies.getJSON('map_zoom')) ? Cookies.getJSON('map_zoom') : 10;
    this.map = L.map(container_id, {
        zoom: map_zoom,
        //origin: [504557, 9755681],
        //crs: L.CRS.EPSG3857,
        //center: new L.LatLng(51.3, 0.7),
        center: map_center,
        // center: new L.LatLng(-245633.38216222863,-9014064.264169712),
        //center:[585422.2807558172,4496946.878818268],
        //layers: [mapquestOSM, boroughs, markerClusters, highlight],
        zoomControl: false,
        attributionControl: true
    });
    var zoomControl = L.control.zoom({
        position: 'topleft'
    });

    this.map.addControl(zoomControl);
    var fullScreenControl = L.control.fullscreen({
        position: 'topleft', // change the position of the button can be topleft, topright, bottomright or bottomleft, defaut topleft
        title: 'Pantalla completa', // change the title of the button, default Full Screen
        content: null, // change the content of the button, can be HTML, default null
        forceSeparateButton: true, // force seperate button to detach from zoom buttons, default false
        forcePseudoFullscreen: true // force use of pseudo full screen even if full screen API is available, default false
    });
    this.map.addControl(fullScreenControl);
    var navBarControl = L.control.navbar({
        position: 'topleft'
    });
    this.map.addControl(navBarControl);
    $('<div class="leaflet-control-coordinates leaflet-control"><div class="uiElement label"><span class="labelFirst" id="coordinate_span">dfd fgd fg dfgd fgd fg </span></div></div>')
            .appendTo('#map .leaflet-bottom.leaflet-left');
    window.coordinates = $('#coordinate_span');
    this.map.on("moveend", function (e) {
        if (e.target._animateToCenter) {
            Cookies.set('map_center_lat', e.target._animateToCenter.lat);
            Cookies.set('map_center_lng', e.target._animateToCenter.lng);
        }
        if (e.target._animateToZoom) {
            Cookies.set('map_zoom', e.target._animateToZoom);
            console.log(e.target._animateToZoom);
        }

    });
    this.map.on("mousemove", function (e) {
        var latlon = e.latlng;
        var bngcoords = proj4(Constants.proj4js.epsg_4326, Constants.proj4js.epsg_32717, [latlon.lat, latlon.lng]);
        window.coordinates.html('Lat: ' + bngcoords[0] + " , Lon: " + bngcoords[1]);
    });


};
//<editor-fold defaultstate="collapsed" desc="BBoxString">
MapManager.prototype.BBoxString = function () {
    var bounds = this.map.getBounds();
    return '' + bounds.getSouth() + ',' + bounds.getWest() + ',' + bounds.getNorth() + ',' + bounds.getEast();
}
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="findLayer">
MapManager.prototype.findLayer = function (title) {
    var a = null;
    for (var key in this.visibleLayers) {
        if (this.visibleLayers.hasOwnProperty(key)) {
            if (key === title) {
                a = this.visibleLayers[key];
                break;
            }
        }
    }
    return a;
};
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="adicionarCapa">
MapManager.prototype.adicionarCapa = function (layer_name, index, color) {
    switch (layer_name) {
        case 'predios':
            this.crearCapaPredios(index);
            if (this.visibleLayers.predios) {
                this.visibleLayers.predios.addTo(this.map);
            }
            break;
        case 'manzanas':
            this.crearCapaBatimetria(index);
            if (this.visibleLayers.batimetria) {
                this.visibleLayers.batimetria.addTo(this.map);
            }
            break;    
        case 'zonas':
            this.createHospitalesLayer(index, color);
            if (this.visibleLayers.hospitales) {
                this.visibleLayers.hospitales.addTo(this.map);
            }
            break;
        case 'vias_comunicacion':
            this.crearCapaHoteles(index, color);
            if (this.visibleLayers.hoteles) {
                this.visibleLayers.hoteles.addTo(this.map);
            }
            break;
        case 'sectores':
            this.crearCapaUniversidades(index, color);
            if (this.visibleLayers.universidades) {
                this.visibleLayers.universidades.addTo(this.map);
            }
            break;
        case 'osm':
            layer = L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
                attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
            });
            layer.setZIndex(index);
            layer.addTo(this.map);
            break;
        case 'ortofoto':
            layer = L.tileLayer.wms("http://localhost:8081/geoserver/Salinas/wms", {
                layers: 'Salinas:ortofoto',
                format: 'image/png',
                transparent: false,
                version: '1.1.0',
                maxZoom: 20,
                attribution: "Ortofoto Salinas"
            });
            layer.setZIndex(index);
            layer.addTo(this.map);
            break;
    }
};
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="removeLayer">
MapManager.prototype.removeLayer = function (layer_name) {
    var layer = this.findLayer(layer_name);
    if (layer) {
        this.map.removeLayer(layer);
    }
};
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="cargarBatimetria">
MapManager.prototype.cargarBatimetria = function (response) {
    var layer = this.findLayer('batimetria');
    if (layer) {
        layer.addData(response);
    }
};
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="crearCapaPredios">
MapManager.prototype.crearCapaPredios = function (index) {
 var defaultStyle = {
        color: "#2262CC",
        weight: 2,
        opacity: 0.6,
        fillOpacity: 0.1,
        fillColor: "#2262CC"
    };
    var highlightStyle = {
        color: '#2262CC',
        weight: 3,
        opacity: 0.6,
        fillOpacity: 0.65,
        fillColor: '#2262CC'
    };
    var mapa = this.map;
    var layer = new L.geoJson(null, {
        onEachFeature: function (feature, layer) {
            // Create a self-invoking function that passes in the layer
            // and the properties associated with this particular record.
            layer.on('click', function (e) {

                //alert(feature.properties.text);
                var url = Routing.generate('informacion-predio', {
                    clave: feature.properties.text
                });
                $.ajax({
                    url: url,
                    timeout: 5000,
                    beforeSend: function () {
                        window.mapManager.showSpin();
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        $("#modal-alert-title").html("Error");
                        $("#modal-alert-body").html("<p>Error al cargar capa</p>");
                        $("#alertModal").modal("show");
                        window.mapManager.hideSpin();
                    },
                    success: function (content) {
                        window.mapManager.hideSpin();
                        $("#feature-title").html("Informaci&oacute;n de solar/predio");
                        $("#feature-info").html(content);
                        $("#featureModal").modal("show");
                    }
                });
            });
            (function (layer, properties) {
                // Create a mouseover event
                layer.on("mouseover", function (e) {
                    // Change the style to the highlighted version
                    layer.setStyle(highlightStyle);
                });
                // Create a mouseout event that undoes the mouseover changes
                layer.on("mouseout", function (e) {
                    // Start by reverting the style back
                    layer.setStyle(defaultStyle);
                });
            })(layer, feature.properties);
        },
        style: function (feature) {
            return defaultStyle;
        }
    });
    var defaultP = $.extend({}, this.defaultLoadParameters);
    var parameters = $.extend(defaultP, {
        typeName: Constants.geoserver.workspace + ':predios',
        format_options: 'callback:mapManager.cargarPredios',
        bbox: this.BBoxString()
    });
    $.ajax({
        url: Constants.geoserver.url + 'wfs' + L.Util.getParamString(parameters),
        dataType: 'jsonp',
        jsonp: false,
        timeout: 5000,
        beforeSend: function () {
            window.mapManager.showSpin();
        },
        success: function (data, textStatus, jqXHR) {
            window.mapManager.hideSpin();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if(jqXHR.readyState!=4){
            $("#modal-alert-title").html("Error");
            $("#modal-alert-body").html("<p>Error al cargar capa</p>");
            $("#alertModal").modal("show");
        }
            window.mapManager.hideSpin();
        }
    });
    layer.setZIndex(index);
    this.visibleLayers.predios = layer;
};
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="cargarPredios">
MapManager.prototype.cargarPredios = function (response) {
    var layer = this.findLayer('predios');
    if (layer) {
        layer.addData(response);
    }
};
//</editor-fold>
//<editor-fold defaultstate="collapsed" desc="crearCapaBatimetria">
MapManager.prototype.crearCapaBatimetria = function (index) {

    var defaultStyle = {color: "#2262CC", weight: 2, opacity: 0.6, fillOpacity: 0.1, fillColor: "#2262CC"};
    var highlightStyle = {color: '#2262CC', weight: 3, opacity: 0.6, fillOpacity: 0.65, fillColor: '#2262CC'};
    var defaultStyle1 = {color: "#27ea41", weight: 2, opacity: 0.6, fillOpacity: 0.1, fillColor: "#27ea41"};
    var highlightStyle1 = {color: '#27ea41', weight: 3, opacity: 0.6, fillOpacity: 0.65, fillColor: '#27ea41'};
    var defaultStyle2 = {color: "#faff00", weight: 2, opacity: 0.6, fillOpacity: 0.1, fillColor: "#faff00"};
    var highlightStyle2 = {color: '#faff00', weight: 3, opacity: 0.6, fillOpacity: 0.65, fillColor: '#faff00'};

    var mapa = this.map;
    var layer = new L.geoJson(null, {
        style: function (feature) {

//            switch (feature.properties.zona_cod) {
//                case 'CMR':
//                    return defaultStyle1;
//                case 'ZR2':
//                    return defaultStyle2;
//                default:
//                    return defaultStyle;
//            }
            return defaultStyle;
        }
    });
    var defaultP = $.extend({}, this.defaultLoadParameters);
    var parameters = $.extend(defaultP, {
        typeName: Constants.geoserver.workspace + ':batimetria',
        format_options: 'callback:mapManager.cargarBatimetria',
        bbox: this.BBoxString()
    });
    $.ajax({
        url: Constants.geoserver.url + 'wfs' + L.Util.getParamString(parameters),
        dataType: 'jsonp',
        jsonp: false,
        timeout: 5000,
        beforeSend: function () {
            window.mapManager.showSpin();
        },
        success: function (data, textStatus, jqXHR) {
            window.mapManager.hideSpin();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.readyState != 4) {
                $("#modal-alert-title").html("Error");
                $("#modal-alert-body").html("<p>Error al cargar capa del geoverver</p>");
                $("#alertModal").modal("show");
            }
            window.mapManager.hideSpin();
        }
    });
    layer.setZIndex(index);
    this.visibleLayers.predios = layer;
};
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="cargarHoteles">
MapManager.prototype.cargarHoteles = function (response) {
    var layer = this.findLayer('hoteles');
    if (layer) {
        layer.addData(response);
    }
};
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="crearCapaHoteles">
MapManager.prototype.crearCapaHoteles = function (index, color) {
    var marker = L.AwesomeMarkers.icon({
        icon: 'hotel',
        prefix: 'fa',
        markerColor: color,
        iconColor: '#fff'
    });
    var layer = new L.geoJson(null, {
        onEachFeature: function (feature, layer) {
            if (feature.properties) {
                var content = "<table class='table table-striped table-bordered table-condensed'>" + "<tr><th>Nombre</th><td>" + feature.properties.nombre + "</td></tr>" + "<tr><th>Telefono</th><td>" + feature.properties.telefono + "</td></tr>" + "<tr><th>Direccion</th><td>" + feature.properties.direccion + "</td></tr>" + "<tr><th>Web</th><td><a class='url-break' href='" + feature.properties.URL + "' target='_blank'>" + feature.properties.URL + "</a></td></tr>" + "<table>";
                layer.on({
                    click: function (e) {
                        $("#feature-title").html(feature.properties.nombre);
                        $("#feature-info").html(content);
                        $("#featureModal").modal("show");
                    }
                });
            }
        },
        pointToLayer: function (feature, latlng) {
            return L.marker(latlng, {icon: marker});
        },
    });
    var defaultP = L.Util.extend({}, this.defaultLoadParameters);
    var parameters = L.Util.extend(defaultP, {
        typeName: Constants.geoserver.workspace + ':hoteles',
        format_options: 'callback:mapManager.cargarHoteles'
    });
    $.ajax({
        timeout: 5000,
        url: Constants.geoserver.url + 'wfs' + L.Util.getParamString(parameters),
        dataType: 'jsonp',
        jsonp: false,
        beforeSend: function () {
            window.mapManager.showSpin();
        },
        success: function (data, textStatus, jqXHR) {
            window.mapManager.hideSpin();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.readyState != 4) {
                $("#modal-alert-title").html("Error");
                $("#modal-alert-body").html("<p>Error al cargar capa</p>");
                $("#alertModal").modal("show");
            }
            window.mapManager.hideSpin();
        }
    });
    layer.setZIndex(index);
    this.visibleLayers.hoteles = layer;
};
//</editor-fold>

MapManager.prototype.hideSpin = function () {
    this.map.spin(false);
};
MapManager.prototype.showSpin = function () {
    this.map.spin(true, Constants.loadingSpiner);
};
//<editor-fold defaultstate="collapsed" desc="cargarUniversidades">
MapManager.prototype.cargarUniversidades = function (response) {
    var layer = this.findLayer('universidades');
    if (layer) {
        layer.addData(response);
    }
};
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="crearCapaUniversidades">
MapManager.prototype.crearCapaUniversidades = function (index, color) {
    var marker = L.AwesomeMarkers.icon({
        icon: 'university',
        prefix: 'fa',
        markerColor: color,
        iconColor: '#fff'
    });
    var layer = new L.geoJson(null, {
        onEachFeature: function (feature, layer) {
            if (feature.properties) {
                var content = "<table class='table table-striped table-bordered table-condensed'>" + "<tr><th>Nombre</th><td>" + feature.properties.nombre + "</td></tr>" + "<tr><th>Telefono</th><td>" + feature.properties.telefono + "</td></tr>" + "<tr><th>Direccion</th><td>" + feature.properties.direccion + "</td></tr>" + "<tr><th>Web</th><td><a class='url-break' href='" + feature.properties.URL + "' target='_blank'>" + feature.properties.URL + "</a></td></tr>" + "<table>";
                layer.on({
                    click: function (e) {
                        $("#feature-title").html(feature.properties.nombre);
                        $("#feature-info").html(content);
                        $("#featureModal").modal("show");
                    }
                });
            }
        },
        pointToLayer: function (feature, latlng) {
            return L.marker(latlng, {icon: marker});
        },
    });
    var defaultP = $.extend({}, this.defaultLoadParameters);
    var parameters = $.extend(defaultP, {
        typeName: Constants.geoserver.workspace + ':universidades',
        format_options: 'callback:mapManager.cargarUniversidades'
    });
    $.ajax({
        url: Constants.geoserver.url + 'wfs' + L.Util.getParamString(parameters),
        dataType: 'jsonp',
        timeout: 5000,
        jsonp: false,
        beforeSend: function () {
            window.mapManager.showSpin();
        },
        success: function (data, textStatus, jqXHR) {
            window.mapManager.hideSpin();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.readyState != 4) {
                $("#modal-alert-title").html("Error");
                $("#modal-alert-body").html("<p>Error al cargar capa</p>");
                $("#alertModal").modal("show");
            }
            window.mapManager.hideSpin();
        }
    });
    layer.setZIndex(index);
    this.visibleLayers.universidades = layer;
};
//</editor-fold>


MapManager.prototype.createHospitalesLayer = function () {

    var geojsonFeature = {
        "type": "Feature",
        "properties": {
            "name": "Coors Field",
            "amenity": "Baseball Stadium",
            "popupContent": "This is where the Rockies play!"
        },
        "geometry": {
            "type": "Point",
            "coordinates": [-80.97826825012811, -2.202897189983574]
        }
    };
    var redMarker = L.AwesomeMarkers.icon({
        icon: 'h-square',
        prefix: 'fa',
        markerColor: 'green',
        iconColor: '#fff'
    });
    layer = L.geoJson(geojsonFeature, {
        pointToLayer: function (feature, latlng) {
            return L.marker(latlng, {icon: redMarker});
        },
        onEachFeature: function (feature, layer) {

        }

    });
    layer.setZIndex(index);
    this.visibleLayers.hospitales = layer;
};






