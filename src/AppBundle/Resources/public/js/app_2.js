$(document).ready(function () {
    $.cookieBar({
        message: 'Este sitio usa cookies para guardar datos de preferencias',
        acceptButton: true,
        acceptText: 'Aceptar',
        declineButton: true,
        declineText: 'Desabilitar Cookies',
        expireDays: 365,
        renewOnVisit: false,
        zindex: 300,
        forceShow: false,
        effect: 'slide',
        element: 'body',
        append: false,
        fixed: true,
        bottom: false
    });

    $('#layersPanel a.layer-btn').click(function (e) {
        e.preventDefault();
        var anchor = $(this);
        layer = anchor.attr('data-layer-name');
        index = anchor.attr('data-layer-index');
        if (anchor.hasClass('layer-off')) {
            anchor.addClass('layer-on');
            anchor.removeClass('layer-off');
            if (layer) {
                toogleLayer(layer, index, true);
            }
        } else if (anchor.hasClass('layer-on')) {
            anchor.addClass('layer-off');
            anchor.removeClass('layer-on');
            if (layer) {
                toogleLayer(layer, index, false);
            }
        }
    });


});

function toogleLayer(layer, index, enable) {

}

// Setup the Bootstrap tabs.
/*$('#tabs a').click(function (e) {
 e.preventDefault();
 $(this).tab('show');
 });*/

// Setup the off-canvas toggle button for smaller screens.
$("[data-toggle=offcanvas]").click(function () {
    $(".row-offcanvas").toggleClass('active');
});

/** 
 * Set the height of the map div.
 */
function setMapDivHeight() {
    var topNavBar, mapDiv, desiredHeight, sidebarDiv;

    topNavBar = document.getElementById("topNavBar");
    mapDiv = document.getElementById("map");
    sidebarDiv = document.getElementById("sidebar");

    desiredHeight = window.innerHeight - topNavBar.clientHeight - 40;
    desiredHeight = [desiredHeight, "px"].join("");

    mapDiv.style.height = desiredHeight;
    sidebarDiv.style.height = desiredHeight;

    var tabPanes = document.querySelectorAll(".tab-pane");

    desiredHeight = window.innerHeight - topNavBar.clientHeight - 80;
    desiredHeight = [desiredHeight, "px"].join("");

    for (var i = 0, l = tabPanes.length; i < l; i += 1) {
        tabPanes[i].style.height = desiredHeight;
    }
}

// Setup map resizing code handlers for when the browser is resized or the device is rotated.
setMapDivHeight();
window.addEventListener("resize", setMapDivHeight, true);
window.addEventListener("deviceorientation", setMapDivHeight, true);



proj4.defs("EPSG:32717", "+proj=utm +zone=17 +south +datum=WGS84 +units=m +no_defs");
var proj_32717 = new ol.proj.Projection({
    code: 'EPSG:32717',
    extent: [441867.78, 1116915.04, 833978.56, 10000000.00]
});

center_900913 = [-9014470.335882477, -245298.97016348102];
center_32717 = [502521.1127400292, 9756603.191879256];
center_3857 = [-9014064.264169712, -245633.38216222863];

/*EPSG:900913*/

var map_center = null;

if (Cookies.getJSON('map_center')) {
    map_center = Cookies.getJSON('map_center').valor;
}


var map_resolution = null;

if (Cookies.getJSON('map_resolution')) {
    map_resolution = Cookies.getJSON('map_resolution').valor;
}


if (!map_center) {
    map_center = center_3857;
}

var map_view = new ol.View({
    projection: 'EPSG:3857',
    center: map_center
});

if (!map_resolution) {
    map_view.setZoom(14);
} else {
    map_view.setResolution(map_resolution);
}

map = new ol.Map({
    target: 'map',
    renderer: 'canvas',
    view: map_view
});

var selectedFeature = null;

// select interaction working on "pointermove"
var selectPointerMove = new ol.interaction.Select({
    condition: ol.events.condition.pointerMove
});
map.addInteraction(selectPointerMove);
selectPointerMove.on('select', function (e) {
    if (e.target.getFeatures().getLength() > 0) {
        selectedFeature = e.target.getFeatures().item(0);
        // console.log(e.target.getFeatures().item(0).getProperties().area);
    } else {
        selectedFeature = null;
    }

});

map.getView().on('change:resolution', function (e) {
    Cookies.set('map_resolution', {valor: e.target.get(e.key)});
});

map.getView().on('change:center', function (e) {
    Cookies.set('map_center', {valor: e.target.get(e.key)});
});



//Full Screen
var myFullScreenControl = new ol.control.FullScreen();
map.addControl(myFullScreenControl);

//Rotate
var myRotateControl = new ol.control.Rotate()
map.addControl(myRotateControl);

//ScaleLine
var myScaleLine = new ol.control.ScaleLine()
map.addControl(myScaleLine);
//I often use the scale line. The default implementation looks nice.

//Zoom
var myZoom = new ol.control.Zoom();
map.addControl(myZoom);
//Zoom is a default control, but there are some parameters you could change if you wanted:
//Check them out here: http://ol3js.org/en/master/apidoc/ol.control.Zoom.html


//ZoomSlider
/*
var myZoomSlider = new ol.control.ZoomSlider();
map.addControl(myZoomSlider);
*/
//The zoom slider is a nice addition to your map. It is wise to have it accompany your zoom buttons.



map.on('singleclick', function (evt) {
    var coord = evt.coordinate;
    var transformed_coordinate = ol.proj.transform(coord, "EPSG:900913", "EPSG:3857");
    //console.log(transformed_coordinate);
    if (selectedFeature != null) {
        console.log(selectedFeature.getProperties().area);
    }
})


var newLayer = new ol.layer.Tile({
    source: new ol.source.OSM()
});

var vectorLayer = new ol.layer.Tile({
    source: new ol.source.TileWMS({
        preload: Infinity,
        url: 'http://localhost:8080/geoserver/wms',
        serverType: 'geoserver',
        params: {
            'LAYERS': "Salinas:predios", 'TILED': true
        }
    })
});

//map.addLayer(newLayer);
//map.addLayer(vectorLayer);




var geojsonFormat = new ol.format.GeoJSON();
var vectorSource = new ol.source.Vector({
    loader: function (extent, resolution, projection) {
        var url = 'http://localhost:8080/geoserver/wfs?service=WFS&' +
                'version=1.1.0&request=GetFeature&typename=Salinas:predios&' +
                'outputFormat=text/javascript&format_options=callback:loadFeatures' +
                '&srsname=EPSG:3857&bbox=' + extent.join(',') + ',EPSG:3857';
        // use jsonp: false to prevent jQuery from adding the "callback"
        // parameter to the URL
        $.ajax({url: url, dataType: 'jsonp', jsonp: false});
    },
    strategy: ol.loadingstrategy.tile(ol.tilegrid.createXYZ({
        maxZoom: 19
    }))
});

/*
 strategy: ol.loadingstrategy.bbox
 */
/*
 strategy: ol.loadingstrategy.tile(ol.tilegrid.createXYZ({
 maxZoom: 19
 }))*/
window.loadFeatures = function (response) {
    vectorSource.addFeatures(geojsonFormat.readFeatures(response));
};


var predios = new ol.layer.Vector({
    source: vectorSource,
    style: new ol.style.Style({
        stroke: new ol.style.Stroke({
            color: 'rgba(0, 0, 255, 1.0)',
            width: 2
        })
    })
});




map.addLayer(predios);


//center: ol.proj.transform([-75.923853, 45.428736], 'EPSG:4326', 'EPSG:3857'),
