// Setup the Bootstrap tabs.
$('#tabs a').click(function (e) {
    e.preventDefault();
    $(this).tab('show');
});

// Setup the off-canvas toggle button for smaller screens.
$("[data-toggle=offcanvas]").click(function () {
    $(".row-offcanvas").toggleClass('active');
});

/** 
 * Set the height of the map div.
 */
function setMapDivHeight() {
    var topNavBar, mapDiv, desiredHeight, sidebarDiv;

    topNavBar = document.getElementById("topNavBar");
    mapDiv = document.getElementById("map");
    sidebarDiv = document.getElementById("sidebar");

    desiredHeight = window.innerHeight - topNavBar.clientHeight - 40;
    desiredHeight = [desiredHeight, "px"].join("");

    mapDiv.style.height = desiredHeight;
    sidebarDiv.style.height = desiredHeight;

    var tabPanes = document.querySelectorAll(".tab-pane");

    desiredHeight = window.innerHeight - topNavBar.clientHeight - 80;
    desiredHeight = [desiredHeight, "px"].join("");

    for (var i = 0, l = tabPanes.length; i < l; i += 1) {
        tabPanes[i].style.height = desiredHeight;
    }
}

// Setup map resizing code handlers for when the browser is resized or the device is rotated.
setMapDivHeight();
window.addEventListener("resize", setMapDivHeight, true);
window.addEventListener("deviceorientation", setMapDivHeight, true);



/* global L, Bloodhound, Handlebars */

var map, boroughSearch = [], theaterSearch = [], museumSearch = [];

$(window).resize(function () {
    sizeLayerControl();
});



function sizeLayerControl() {
    $(".leaflet-control-layers").css("max-height", $("#map").height() - 50);
}


/*
 var crs = new L.Proj.CRS('EPSG:32717',
 '+proj=utm +zone=17 +south +datum=WGS84 +units=m +no_defs',
 {
 resolutions: [8192, 4096, 2048, 1024, 512, 256, 128, 64, 32, 16, 8, 4, 2, 1, 0.5] // 3 example zoom level resolutions
 }
 );
 */
map = L.map("map", {
    //zoom: 10,
    //origin: [504557, 9755681],
    //crs:crs,
    //center: [40.702222, -73.979378],

    //center:[585422.2807558172,4496946.878818268],
    //layers: [mapquestOSM, boroughs, markerClusters, highlight],
    zoomControl: false,
    attributionControl: true
});

map.setView([39.74739, -105], 15);

var campus = {
    "type": "Feature",
    "properties": {
        "popupContent": "This is the Auraria West Campus",
        "style": {
            weight: 2,
            color: "#999",
            opacity: 1,
            fillColor: "#B0DE5C",
            fillOpacity: 0.8
        }
    },
    "geometry": {
        "type": "MultiPolygon",
        "coordinates": [
            [
                [
                    [-105.00432014465332, 39.74732195489861],
                    [-105.00715255737305, 39.74620006835170],
                    [-105.00921249389647, 39.74468219277038],
                    [-105.01067161560059, 39.74362625960105],
                    [-105.01195907592773, 39.74290029616054],
                    [-105.00989913940431, 39.74078835902781],
                    [-105.00758171081543, 39.74059036160317],
                    [-105.00346183776855, 39.74059036160317],
                    [-105.00097274780272, 39.74059036160317],
                    [-105.00062942504881, 39.74072235994946],
                    [-105.00020027160645, 39.74191033368865],
                    [-105.00071525573731, 39.74276830198601],
                    [-105.00097274780272, 39.74369225589818],
                    [-105.00097274780272, 39.74461619742136],
                    [-105.00123023986816, 39.74534214278395],
                    [-105.00183105468751, 39.74613407445653],
                    [-105.00432014465332, 39.74732195489861]
                ],[
                    [-105.00361204147337, 39.74354376414072],
                    [-105.00301122665405, 39.74278480127163],
                    [-105.00221729278564, 39.74316428375108],
                    [-105.00283956527711, 39.74390674342741],
                    [-105.00361204147337, 39.74354376414072]
                ]
            ],[
                [
                    [-105.00942707061768, 39.73989736613708],
                    [-105.00942707061768, 39.73910536278566],
                    [-105.00685214996338, 39.73923736397631],
                    [-105.00384807586671, 39.73910536278566],
                    [-105.00174522399902, 39.73903936209552],
                    [-105.00041484832764, 39.73910536278566],
                    [-105.00041484832764, 39.73979836621592],
                    [-105.00535011291504, 39.73986436617916],
                    [-105.00942707061768, 39.73989736613708]
                ]
            ]
        ]
    }
};

//L.geoJson(geojsonFeature).addTo(map);
/*
 [-2.20054141024922,-80.9779443332566],
 [-2.20070573850612,-80.9780380417333],
 [-2.2006851116907,-80.978088944314],
 [-2.20067497921926,-80.9781139459344],
 [-2.20062458826053,-80.9782382345608],
 [-2.2004581996827,-80.9781529924229],
 [-2.20054141024922,-80.9779443332566]
 
 
 [-80.9779443332566, -2.20054141024922],
 [-80.9780380417333, -2.20070573850612],
 [-80.978088944314, -2.2006851116907],
 [-80.9781139459344, -2.20067497921926],
 [-80.9782382345608, -2.20062458826053],
 [-80.9781529924229, -2.2004581996827],
 [-80.9779443332566, -2.20054141024922]
 
 **/

var geojsonFeature2 = {
    "type": "Feature",
    "properties": {
        "name": "Coors Field",
        "amenity": "Baseball Stadium",
        "popupContent": "This is where the Rockies play!"
    },
    "geometry": {
        "type": "Point",
        "coordinates": [-104.99404, 39.75621]
    }
};

var baseballIcon = L.icon({
    iconUrl: 'baseball-marker.png',
    iconSize: [32, 37],
    iconAnchor: [16, 37],
    popupAnchor: [0, -28]
});

/*
var geojsonLayer = new L.GeoJSON(geojsonFeature2, {
    pointToLayer: function (feature, latlng) {
        return L.marker(latlng, {icon: baseballIcon});
    },
});
map.addLayer(geojsonLayer);*/

var geojsonLayer2 = new L.GeoJSON(campus);
map.addLayer(geojsonLayer2);


function onEachFeature(feature, layer) {
    var popupContent = "<p>I started out as a GeoJSON " +
            feature.geometry.type + ", but now I'm a Leaflet vector!</p>";

    if (feature.properties && feature.properties.popupContent) {
        popupContent += feature.properties.popupContent;
    }

    layer.bindPopup(popupContent);
}
;


layers = {};

$("#layersPanwwwel input").each(function (index, input) {
    var workspace = $(this).attr('data-workspace');
    var layerName = $(this).attr('data-layer');

    var layerUrl = "http://localhost:8080/geoserver/" + workspace + "/wms";
    var layer = L.tileLayer.wms(layerUrl, {
        layers: layerName,
        format: 'image/png',
        transparent: true,
        onEachFeature: onEachFeature,
        version: '1.1.0',
        attribution: "myattribution"
    });
    layers[layerName] = layer;
});



var rootUrl = 'http://localhost:8080/geoserver/ows';

var defaultParameters = {
    service: 'WFS',
    version: '1.1.0',
    request: 'GetFeature',
    typeName: 'Salinas:prueba',
    maxFeatures: 200,
    outputFormat: 'text/javascript',
    format_options: 'callback: getJson'

};

//var parameters = L.Util.extend(defaultParameters);
/*
 $.ajax({
 url: rootUrl + L.Util.getParamString(parameters),
 dataType: 'jsonp',
 jsonpCallback: 'getJson',
 success: handleJson
 });
 
 function handleJson(data) {
 L.geoJson(data, {
 onEachFeature: onEachFeature,
 pointToLayer: function (feature, latlng) {
 //return L.circleMarker(latlng, geojsonMarkerOptions);
 //return L.marker(latlng);
 console.log(feature)
 }
 }).addTo(map);
 }*/


function loadGeoJson(data) {
    console.log(data);
    var geojsonLayer = L.geoJson(data, {
        onEachFeature: function (feature, layer) {
            console.log(feature.properties);
        },
        complete: function (layers) {
            console.log(layers);
        }
    });
    //var jsonGroup = new L.FeatureGroup(data);
    //map.fitBounds(geojsonLayer.getBounds());
    map.addLayer(geojsonLayer);
    //geojsonLayer.addData(data);

}

/*
 $.ajax('http://localhost:8080/geoserver/ows',{
 type: 'GET',
 data: {
 service: 'WFS',
 version: '1.1.0',
 request: 'GetFeature',
 typeName: 'Salinas:prueba',
 srsName: 'EPSG:4326',
 maxFeatures:'5',
 outputFormat: 'text/javascript',
 format_options:'callback:loadGeoJson'
 },
 dataType: 'jsonp',
 jsonpCallback:'callback:loadGeoJson'
 });
 */

$("#layerssssPanel input").click(function (index, input) {
    /*var selected = $(this).prop('checked');
     
     var layerName = $(this).attr('data-layer');
     var layerIndex = $(this).attr('data-index');
     
     var layer = layers[layerName];
     if (selected) {
     layer.setZIndex(layerIndex).addTo(map);
     } else
     {
     map.removeLayer(layer);
     }*/






});







var zoomControl = L.control.zoom({
    position: "topleft"
}).addTo(map);

var measureControl = L.Control.measureControl({
    position: "topleft"
}).addTo(map);

// create a fullscreen button and add it to the map
var fullScreenControl = L.control.fullscreen({
    position: 'topleft', // change the position of the button can be topleft, topright, bottomright or bottomleft, defaut topleft
    title: 'Pantalla completa', // change the title of the button, default Full Screen
    content: null, // change the content of the button, can be HTML, default null
    forceSeparateButton: true, // force seperate button to detach from zoom buttons, default false
    forcePseudoFullscreen: true // force use of pseudo full screen even if full screen API is available, default false
}).addTo(map);

/* GPS enabled geolocation control set to follow the user's location */
/*
 var locateControl = L.control.locate({
 position: "topleft",
 drawCircle: true,
 follow: true,
 setView: true,
 keepCurrentZoomLevel: true,
 markerStyle: {
 weight: 1,
 opacity: 0.8,
 fillOpacity: 0.8
 },
 circleStyle: {
 weight: 1,
 clickable: false
 },
 icon: "icon-direction",
 metric: false,
 strings: {
 title: "My location",
 popup: "You are within {distance} {unit} from this point",
 outsideMapBoundsMsg: "You seem located outside the boundaries of the map"
 },
 locateOptions: {
 maxZoom: 18,
 watch: true,
 enableHighAccuracy: true,
 maximumAge: 10000,
 timeout: 10000
 }
 }).addTo(map);
 
 */


/* Larger screens get expanded layer control and visible sidebar */
if (document.body.clientWidth <= 767) {
    var isCollapsed = true;
} else {
    var isCollapsed = false;
}





/* Typeahead search functionality */
/*$(document).one("ajaxStop", function () {
 $("#loading").hide();
 sizeLayerControl();
 
 //map.fitBounds(boroughs.getBounds());
 
 
 
 });*/

// Leaflet patch to make layer control scrollable on touch browsers
/*var container = $(".leaflet-control-layers")[0];
 if (!L.Browser.touch) {
 L.DomEvent
 .disableClickPropagation(container)
 .disableScrollPropagation(container);
 } else {
 L.DomEvent.disableClickPropagation(container);
 }*/