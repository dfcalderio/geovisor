$(document).ready(function () {

    $('#layersGroupsForm').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
        dataType:  'json', 
 
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   processJson 
    }); 



    $.ajax({
        url: "http://localhost:8080/geoserver/rest/workspaces",
        dataType: 'jsonp',
        success: function (results) {
            var title = results.response.oneforty;
            var numTweets = results.response.trackback_total;
            $('#results').append(title + ' has ' + numTweets + ' tweets.');
        }
    });


});


